import React from 'react';

import 'intl';
import 'intl/locale-data/jsonp/en'; // or any other locale you need
import 'intl/locale-data/jsonp/es'; // or any other locale you need

import Navigation from './Navigation';
import Loading from './components/Loading';
import init from './config/setup';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { Provider } from 'react-redux';
import store from './redux/store';


import { StyleSheet, Text, View, YellowBox, StatusBar } from 'react-native';

class App extends React.Component {

  constructor(props){
    super(props)
    YellowBox.ignoreWarnings(['Setting a timer']);
    this.state = {
      loading     : true,
      verifyEmail : true
    }
  }

  componentDidMount() {

    this.start();
  }

  start = async () => {
    this.unsuscribeAuth = firebase.auth().onAuthStateChanged(user => {

      if(user){

        firebase.firestore().collection('users').doc(user.uid).get().then(doc => {
          store.dispatch({ type: 'UPDATE_AUTH_STATE', payload : {
            'loggedIn' : true, 'uid' : user.uid, emailVerified : user.emailVerified, ...doc.data()
          }})
        }).then(r => {
          this.setState({ loading : false })
        }).catch(e => console.log(e))

      } else {

        store.dispatch({'type' : 'INIT_AUTH_STATE'});
        this.setState({ loading : false });
      }
    })
  }

  componentWillUnmount(){
    this.unsuscribeAuth();
  }

  renderApp = store =>
    <Provider store={store}>
      <StatusBar barStyle="light-content" />
      <Navigation/>
    </Provider>

  render(){
    return !this.state.loading ? this.renderApp(store) : <Loading/>
  }
}

export default App;