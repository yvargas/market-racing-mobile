import React from 'react';
import {
  StyleSheet, StatusBar, Platform
} from 'react-native';

import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { colors, theme }  from './assets/theme';

import { Ionicons } from '@expo/vector-icons';

import Login from './screens/auth/Login';
import SignUp from './screens/auth/SignUp';
import VerifyEmail from './screens/auth/VerifyEmail';
import Sales from './screens/sales/Sales';
import SaleDetail from './screens/sales/SaleDetail';
import Directory from './screens/directory/Directory';
import DirectoryByCategory from './screens/directory/DirectoryByCategory';
import DirectoryDetail from './screens/directory/DirectoryDetail';
import Saved from './screens/Saved';
import Post from './screens/Post';
import Profile from './screens//profile/Profile';
import EditSale from './screens//profile/EditSale';
import ProfileEdit from './screens//profile/ProfileEdit';

const Navigation = props => {

  const Tab   = createBottomTabNavigator();
  const Stack = createStackNavigator();
  const { auth } = props;

  const initialAuth = (auth.loggedIn && !auth.emailVerified)? 'VerifyEmail' : 'Login';

  const unAuthUsers = () => {
    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName={initialAuth}
          screenOptions={{
            headerShown: false
          }}>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="VerifyEmail" component={VerifyEmail} />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }

  const SalesNavigator = () => {
    return (
      <Stack.Navigator
        initialRouteName='Sale'
        screenOptions={{
          headerShown: false
        }}>
        <Stack.Screen name="Sale" component={Sales} />
        <Stack.Screen name="SaleDetail" component={SaleDetail} />
      </Stack.Navigator>
    );
  }

  const DirectoryNavigator = () => {
    return (
      <Stack.Navigator
        initialRouteName='Directory'
        screenOptions={{
          headerShown: false
        }}>
        <Stack.Screen name="Directory" component={Directory} />
        <Stack.Screen name="DirectoryByCategory" component={DirectoryByCategory} />
        <Stack.Screen name="DirectoryDetail" component={DirectoryDetail} />
      </Stack.Navigator>
    );
  }

  const ProfileNavigator = () => {
    return (
      <Stack.Navigator
        initialRouteName='Profile'
        screenOptions={{
          headerShown: false
        }}>
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="EditSale" component={EditSale} />
        <Stack.Screen name="ProfileEdit" component={ProfileEdit} />
      </Stack.Navigator>
    );
  }

  const authUsers = () => {
    return (
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName={!auth.phoneNumber? 'Perfil' : 'Venta'}
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Venta') {
                iconName = focused
                  ? 'ios-megaphone'
                  : 'ios-megaphone';
              } else if (route.name === 'Directorio') {
                iconName = focused ? 'ios-list' : 'ios-list';
              } else if (route.name === 'Publicar') {
                iconName = focused ? 'ios-add-circle' : 'ios-add-circle-outline';
              } else if (route.name === 'Guardados') {
                iconName = focused ? 'ios-bookmark' : 'ios-bookmark';
              } else if (route.name === 'Perfil') {
                iconName = focused ? 'ios-person' : 'ios-person';
              }

              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },

          })}
          tabBarOptions={{
            headerShown: false,
            activeTintColor: colors.third ,
            inactiveTintColor: 'gray',
            showLabel: false,
            style: {
              backgroundColor: colors.fourth,
              paddingTop: 8
            }
          }}>
          <Tab.Screen name="Venta" component={SalesNavigator} />
          <Tab.Screen name="Directorio" component={DirectoryNavigator} />
          <Tab.Screen name="Publicar" component={Post} />
          <Tab.Screen name="Guardados" component={Saved} />
          <Tab.Screen name="Perfil" component={ProfileNavigator} options={{ tabBarBadge: !auth.phoneNumber? '!' : null }}/>
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
  
  return auth.loggedIn && auth.emailVerified ? authUsers() : unAuthUsers();
}

const mapStateToProps = state => {
  return {
    auth : state.Auth
  }
}

export default connect(mapStateToProps)(Navigation);