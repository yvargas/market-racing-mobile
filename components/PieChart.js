import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";
import { categories } from '../config/setup'

const ViewPieChart = ({data}) => {
  return(
    <PieChart
      data={data}
      width={Dimensions.get('window').width}
      height={290}
      chartConfig={{
        decimalPlaces: 2,
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        style: {
          // borderRadius: 16,
        },
      }}
      style={{
        
      }}
      accessor="population"
      backgroundColor="transparent"
      paddingLeft="30"
    />
  );
}

export default ViewPieChart;

const styles = StyleSheet.create({
  header: {
    // backgroundColor : '#cfe4f3',
  }
});