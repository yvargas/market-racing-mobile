import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";

import Ticker, { Tick } from "react-native-ticker";


const Total = ({total, lastTotal}) => {
  const [state, setState] = useState({
      value: lastTotal
    });

  useEffect(() => {
    setTimeout(() => {
      setState({
        value: total, lastTotal
      });
    },300)
  }, []);

  return (
    <View style={styles.container}>
      <Ticker textStyle={styles.text}>
        {state.value.toLocaleString()}
      </Ticker>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {

  },
  text: {
    textAlign : 'right',
    color : 'white',
    fontSize : 40
  }
});

export default Total;