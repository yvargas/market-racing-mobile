import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';

const Header = ({title, description}) => {

	return(
		<View style={styles.header}>
			<Text style={styles.title}>{title}</Text>
			<Text style={styles.description}>{description}</Text>
		</View>
	);
}

export default Header;

const styles = StyleSheet.create({
  header: {
    height : 200,
    backgroundColor : '#002f50',
    paddingTop: 25,
    paddingHorizontal : 10,
  },
  title : {
  	paddingTop : 30,
  	color : 'white',
  	fontSize : 26
  },
  description : {
  	fontSize : 14,
  	color : '#ddd'
  }
});