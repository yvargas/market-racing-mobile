import { createStore, combineReducers } from 'redux';

import AuthReducer from '../reducers/AuthReducer';
import CategoriesReducer from '../reducers/CategoriesReducer';
import SalesReducer from '../reducers/SalesReducer';
import DirectoryReducer from '../reducers/DirectoryReducer';
import StatsReducer from '../reducers/StatsReducer';

const store = createStore(
  combineReducers({
    Auth 		: AuthReducer,
    Stats 		: StatsReducer,
    Categories 	: CategoriesReducer,
    Sales 		: SalesReducer,
    Directory 	: DirectoryReducer
  })
);

export default store;
