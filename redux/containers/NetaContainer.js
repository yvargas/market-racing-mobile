import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { connect } from 'react-redux';

const BooksCountContainer = ({ type, ...props }) => (
  <View style={styles.container}>
    <Text style={{ color: 'red' }}>{props.length || 0}</Text>
  </View>
);

const mapStateToProps = state => {
  return {
    books: state.books
  };
};

export default connect(mapStateToProps)(BooksCountContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
