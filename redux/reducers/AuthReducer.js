const init = {
  loggedIn        : false,
  emailVerified   : false
};

const AuthReducer = (state = init, action) => {
  switch (action.type) {
    case 'INIT_AUTH_STATE':
    	return { loggedIn: false, emailVerified : false };
    case 'UPDATE_AUTH_STATE':
    	return action.payload;
    case 'INCREMENT_SALE':
      state.sales.push(action.payload)
      return state;
    default:
    	return state;
  }
};

export default AuthReducer;
