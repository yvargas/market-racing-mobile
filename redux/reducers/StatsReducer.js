const init = {
  sales     : 0,
  purchases : 0
};

const AuthReducer = (state = init, action) => {
  switch (action.type) {
    case 'USERS_STATS':
      	return state;
    case 'UPDATE_USER_STATS':
      	return action.payload;
    default:
      	return state;
  }
};

export default AuthReducer;
