const init = {
  list : []
};

const CategoriesReducer = (state = init, action) => {
  switch (action.type) {
    case 'FETCH_CATEGORIES':
      return {
        ...state,
        list : action.payload
      };
    default:
      return state;
  }
};

export default CategoriesReducer;
