const init = {
  list     : []
};

const SalesReducer = (state = init, action) => {
  switch (action.type) {
    case 'FETCH_SALES':
      return {
        ...state,
        list : action.payload
      };
    case 'ADD_SALES':
      return {
        ...state,
        list : state.list.concat(action.payload)
      };
    case 'ADD_SALE':
      return {
        ...state,
        list : action.payload.concat(state.list)
      };
    case 'FETCH_SALES_BY_TERMN':
      return {
        ...state,
        list : action.payload
      };
    case 'ADD_SALES_BY_TERMN':
      return {
        ...state,
        list : state.list.concat(action.payload)
      };
    default:
      return state;
  }
};

export default SalesReducer;
