const init = {
  list : {}
};

const DirectoryReducer = (state = init, action) => {
  switch (action.type) {
    case 'FETCH_DIRECTORY':
      return {
        ...state,
        list : { ...state.list, [action.payload.category] : action.payload.[action.payload.category] }
      };
    case 'ADD_MORE_DIRECTORY':
      return {
        ...state,
        list : { ...state.list, [action.payload.category] : action.payload.[action.payload.category] }
      };
    default:
      return state;
  }
};

export default DirectoryReducer;
