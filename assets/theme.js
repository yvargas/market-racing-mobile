import { StyleSheet } from 'react-native';

const colors = {
  logo : '../../assets/logo.png',
  primary   : '#7935ba',
  secundary : '#7f26de',
  third     : '#5a49df',
  fourth    : '#262938',
  fifth     : '#12141b',
  primaryGradient : ['#5d5a6b', '#131418'],
}

const theme = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#002f50'
  },
  btnPrimary : {
    elevation: 2, // Android
    height: 50,
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,0.2)',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius : 30,
    borderWidth : 1,
    borderColor : 'rgba(255,255,255,0.1)'
  }
});

export { colors, theme };

