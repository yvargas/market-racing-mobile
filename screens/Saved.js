import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, Image} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { colors, theme }  from '../assets/theme';
import { LinearGradient } from 'expo-linear-gradient';

class Tools extends React.Component {

  constructor(){
    super();
    this.state = {
      
    }
  }

  render(){
    return (
      <View style={styles.container}>
        <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
          <View style={styles.headerTitleBox}>
            <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
              <View style={{flex:1}}></View>
              <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
                <Image source={require('../assets/junker.png')} style={{width : 110, height : 20}}/>
              </View>
              <View style={{flex:1}}></View>
            </View>
          </View>
        </LinearGradient>
        
      </View>
    );
  }
}

export default Tools;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.fifth,
  },
  header : {
    backgroundColor : '#002f50',
    paddingTop: 50,
    paddingBottom: 10,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  item: {
    padding: 20,
    borderBottomWidth : 1,
    borderColor : '#ddd'
  }
});
