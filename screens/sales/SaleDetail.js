import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, ScrollView, Dimensions, Image, Linking} from 'react-native';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import Loading from '../../components/Loading';

import { colors, theme }  from '../../assets/theme';
import { LinearGradient } from 'expo-linear-gradient';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import { ActionSheet } from 'react-native-cross-actionsheet'

const { width } = Dimensions.get('window');
const height = width * 0.8;

class SaleDetail extends React.Component {

  constructor(){
    super();
    this.state = {
      index         : null,
      selected      : {},
      slide         : 0
    }
  }

  componentDidMount(){

    const index = this.props.sales.list.findIndex(s => s.id == this.props.route.params.id);
    
    this.setState({selected : this.props.sales.list[index], index});
  }

  updateDot = ({nativeEvent}) => {

    var slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);

    this.setState({slide : slide})
  }

  formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
  })

  call = () => {
    let number = this.state.selected.created_by.phoneNumber;
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else {phoneNumber = `telprompt:${number}`; }
    Linking.openURL(phoneNumber);
  }

  whatsappMsg = () => {
    let number = this.state.selected.created_by.phoneNumber;
    Linking.openURL('whatsapp://send?text='+this.state.selected.title+', publicado en JUNKER APP. Sigue disponible ?&phone='+number)
  }


  callOptions = () => {

    if(this.state.selected.created_by.phoneNumber){

      let options = [
        { text: 'Llamar', onPress: () => this.call() }
      ]

      if(this.state.selected.created_by.contactMethod == 'whatsapp'){
        options = [
          { text: 'Llamar', onPress: () => this.call() },
          { text: 'Escribir al Whatsapp', onPress: () => this.whatsappMsg() }
        ]
      }

      ActionSheet.options({
        title: 'Contactar',
        message: 'Llamar o escribirle via whatsapp',
        options: options,
        cancel: { onPress: () => console.log('cancel') }
      })

    } else {
      alert("Este usuario no tiene numero de contacto")
    }
  }

  render(){
    return (
      <View style={styles.container}>
        <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
          <View style={styles.headerTitleBox}>
            <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{justifyContent:'center', width: 30}}><Ionicons name="ios-arrow-back" size={20} color='white' /></TouchableOpacity>
              
              <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
                <Text style={{textAlign: 'left', fontWeight : 'bold', marginHorizontal : 10, marginVertical: 10, fontSize: 16, color :'white'}}>{this.state.selected.title}</Text>
              </View>
              <TouchableOpacity onPress={() => this.callOptions()} style={{justifyContent:'center', width: 30, flexDirection : 'row-reverse', alignItems: 'center'}}><Ionicons name="ios-call" size={20} color='white' /></TouchableOpacity>
            </View>

          </View>
        </LinearGradient>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{}}>
            { this.state.index !== null ?
                <>
                  <View style={{flexDirection: 'row', padding: 10, flex: 1, position:'absolute', zIndex: 1, backgroundColor: 'rgba(0,0,0,.3)' }}>
                    <View style={{ flex:1, }}><Text style={{ fontWeight: 'bold', color : 'white' }}>{this.state.selected.category}</Text></View>
                    <View style={{ flex:1, flexDirection: 'row-reverse'}}><Text style={{ fontWeight: 'bold', color : 'white' }}>{this.state.selected.created_at_human}</Text></View>
                  </View>
                  <ScrollView pagingEnabled horizontal scrollEventThrottle={10} showsHorizontalScrollIndicator={false} onScroll={(e) => {this.updateDot(e)}} style={{ width, height }}>
                    {
                      this.state.selected.photos.map((img, key) => {
                        return (
                          <Image
                            key={key}
                            source={{uri:img.url}} 
                            style={{flex:1, width : width, height : height}} 
                          />
                        )
                      })
                    }
                  </ScrollView >
                  <View style={{flexDirection: 'row-reverse', marginTop: -37}}>
                    
                    <View style={{ backgroundColor: 'rgba(100, 153, 30, 0.80)', paddingVertical: 10, paddingHorizontal: 20, borderTopLeftRadius:10 }}><Text style={{ fontWeight : 'bold', color: 'white', textAlign:'right',  }}>{this.state.selected.currency} {this.formatter.format(this.state.selected.price)}</Text></View>
                    <View style={{flex:2}}></View>
                  </View>
                  { this.state.selected.photos.length >= 2 &&
                    <View style={[styles.dots]}>
                      {
                        this.state.selected.photos.map((i,k) => {
                          return (
                            <Text key={k} style={k == this.state.slide ? styles.dotActive : styles.dot}>•</Text>
                          )
                        }) 
                      }
                    </View>
                  }
                </>
              : 
                <Loading/>
            }
          </View>
          
          <View style={{ flex:1, paddingTop: 10, paddingHorizontal: 10, marginBottom: 120 }}>
            <View style={{flexDirection: 'row', margin: 10, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#2b2b2b' }}>
              <View style={{ flex:1 }}><Text style={{ color : 'white', fontWeight : 'bold' }}>Condicion</Text></View>
              <View style={{ flex:1, flexDirection: 'row-reverse' }}><Text style={{ color : 'white' }}>{this.state.selected.condition}</Text></View>
            </View>
            <View style={{flexDirection: 'row', margin: 10, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#2b2b2b' }}>
              <View style={{ flex:1 }}><Text style={{ color : 'white', fontWeight : 'bold' }}>Categoria</Text></View>
              <View style={{ flex:1, flexDirection: 'row-reverse' }}><Text style={{ color : 'white' }}>{this.state.selected.category}</Text></View>
            </View>
            <View style={{flexDirection: 'row', margin: 10, paddingBottom: 10 }}>
              <View style={{flex:1 }}><Text style={{ color : 'white', fontWeight: 'bold' }}>Descripcion</Text></View>
            </View>
            <View style={{flexDirection: 'row', margin: 10, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#2b2b2b' }}>
              <View style={{ }}><Text style={{ color : 'white' }}>{this.state.selected.description}</Text></View>
            </View>

            { this.state.index !== null && 
              <TouchableOpacity onPress={() => this.call()} style={{ justifyContent: 'center', alignItems : 'center', paddingVertical: 10, margin : 10, backgroundColor : (!this.state.selected.created_by.phoneNumber)? 'gray' : 'rgba(100, 153, 30, 1)', borderRadius:10 }} disabled={(this.state.selected.created_by.phoneNumber)? false : true} >
                <Text style={{ color: 'white', fontSize :18 }}><Ionicons name="ios-call" size={18} color='white' /></Text>
              </TouchableOpacity>
            }

            { this.state.index !== null && 
              
              this.state.selected.created_by.contactMethod === 'whatsapp' &&
                <TouchableOpacity onPress={() => this.whatsappMsg()} style={{ justifyContent: 'center', alignItems : 'center', paddingVertical: 10, margin : 10, backgroundColor : (!this.state.selected.created_by.phoneNumber)? 'gray' : 'rgba(100, 153, 30, 1)', borderRadius:10 }} disabled={(this.state.selected.created_by.phoneNumber)? false : true} >
                  <Text style={{ color: 'white', fontSize :18 }}><FontAwesome name="whatsapp" size={18} color='white' /></Text>
                </TouchableOpacity>
            }
          </View>
          
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth  : state.Auth,
    sales : state.Sales
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(SaleDetail);

const styles = StyleSheet.create({
  container: {
    flexGrow:1,
    backgroundColor: colors.fifth,
  },
  dotActive : {
    color : 'white',
    fontSize : 24
  },
  dot : {
    color : 'gray',
    fontSize : 24
  },
  dots : {
    marginTop: -30,
    height: 30,
    width: width,
    flexDirection : 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  header : {
    backgroundColor : colors.primary,
    paddingTop: 40,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingHorizontal : 0,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
});
