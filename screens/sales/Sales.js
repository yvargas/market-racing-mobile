import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, ScrollView, ActivityIndicator, TextInput, Image, RefreshControl,TouchableWithoutFeedback, Keyboard} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { colors, theme }  from '../../assets/theme';
import { LinearGradient } from 'expo-linear-gradient';

import moment from "moment";
import 'moment/locale/es'

// import Image from 'react-native-image-progress';
// import * as Progress from 'react-native-progress';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import firebase from 'firebase/app';
import 'firebase/firestore';

class Sales extends React.Component {

  constructor(){
    super();
    this.state = {
      filter            : 'Todas',
      loadingCategories : true,
      loadingSales      : true,
      showFilter        : true,
      refreshing        : false,
      loadingMore       : false,
      searchTermn       : '',
      startAfter        : null,
      limit             : 10
    }

    moment.locale('es-do')
  }

  componentDidMount() {
    this.getCategories();
    this.getSales();
  }

  getCategories = async () => {
    const categories = await firebase
      .firestore()
      .collection('settings')
      .doc('categories')
      .get()
      .catch(e => console.log(error));

    this.props.fetchCategories(categories.data().list);

    this.setState({ loadingCategories: false })
  }

  getSales = async () => {
    const sales = await firebase
      .firestore()
      .collection('sales')
      .where('status','==','public')
      .orderBy('created_at', 'desc')
      .limit(this.state.limit)
      .get()
      .catch(e => console.log(e));

    const _sales = sales.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))

    this.props.fetchSales(_sales);

    const lastVisible = sales.docs[sales.docs.length-1];
    
    this.setState({ loadingSales: false, startAfter: lastVisible})
  }

  searchByTermn = async () => {

    this.setState({ loadingSales: true })

    const sales = await firebase
      .firestore()
      .collection('sales')
      .where('status','==','public')
      .where('keywords', 'array-contains', this.state.searchTermn.toLowerCase())
      .orderBy('created_at', 'desc')
      .get()
      .catch(e => console.log(e));

    const _sales = sales.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))

    this.props.fetchSalesByTermn(_sales);
    
    this.setState({ loadingSales: false })
  }

  toggleFilter = () => this.setState({ showFilter : !this.state.showFilter })

  toggleCategory = category => (this.state.filter === category)? this.setState({filter : 'Todas'}) : this.setState({filter : category})

  formatter = new Intl.NumberFormat('en-US', {
    style                   : 'currency',
    currency                : 'USD',
    minimumFractionDigits   : 0
  })

  onRefresh = async () => {

    this.setState({refreshing : true, searchTermn : ''});

    const sales = await firebase
      .firestore()
      .collection('sales')
      .where('status','==','public')
      .orderBy('created_at', 'desc')
      .limit(this.state.limit)
      .get()
      .catch(e => console.log(e));

    const _sales = sales.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))

    this.props.fetchSales(_sales);

    this.setState({refreshing : false, startAfter: sales.docs[sales.docs.length-1]});
  };

  clearFilter = () => {

    this.setState({ searchTermn: '' })
    Keyboard.dismiss();

  }

  getMore = async () => {
    
    if(!this.state.refreshing && this.state.startAfter != undefined && this.state.searchTermn === ''){

      this.setState({loadingMore : true});

      this.setState({refreshing : true});

      const sales = await firebase
        .firestore()
        .collection('sales')
        .orderBy('created_at', 'desc')
        .where('status','==','public')
        .startAfter(this.state.startAfter)
        .limit(this.state.limit)
        .get()
        .catch(e => console.log(e));

      const _sales = sales.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))

      this.props.addSales(_sales);

      const lastVisible = sales.docs[sales.docs.length-1];

      this.setState({loadingMore : false, refreshing: false, startAfter: lastVisible});
    
    } else if(!this.state.refreshing && this.state.startAfter != undefined && this.state.searchTermn !== '' && this.props.sales.list.length >= 5){

      this.setState({loadingMore : true});

      this.setState({refreshing : true});

      const sales = await firebase
        .firestore()
        .collection('sales')
        .orderBy('created_at', 'desc')
        .where('status','==','public')
        .where('keywords', 'array-contains', this.state.searchTermn.toLowerCase())
        .startAfter(this.state.startAfter)
        .limit(this.state.limit)
        .get()
        .catch(e => console.log(e));

      const _sales = sales.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))

      this.props.addSalesByTermn(_sales);

      const lastVisible = sales.docs[sales.docs.length-1];

      this.setState({loadingMore : false, refreshing: false, startAfter: lastVisible});
    }
  }

  render(){
    return (
      <View style={styles.container}>
        <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
          <View style={styles.headerTitleBox}>
            <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
              <View style={{flex:1}}></View>
              
              <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
                <Image source={require('../../assets/junker.png')} style={{width : 110, height : 20}}/>
              </View>
              <View style={{flex:1}}></View>
            </View>

            <View style={{flexDirection:'row'}}>
              <TextInput
                style={ styles.searchBar }
                value={this.state.searchTermn}
                placeholder="Buscar"
                onChangeText={text => this.setState({ searchTermn: text })}
                returnKeyType="done"
                onSubmitEditing={this.searchByTermn}
              />
              { this.state.searchTermn !== '' &&
                <TouchableOpacity onPress={() => this.clearFilter()} style={{position: 'absolute', top: 14, right: 50, justifyContent:'center', paddingRight: 20}}><Ionicons name="ios-backspace" size={30} color='gray'/></TouchableOpacity> 
              }
              
              <TouchableOpacity onPress={() => this.toggleFilter()} style={styles.filter}><Ionicons name="ios-funnel" size={20} color='white' /></TouchableOpacity>
            </View>
            
          </View>
        </LinearGradient>

        { this.state.showFilter &&
          <View style={styles.categories}>
            { !this.state.loadingCategories &&
              <ScrollView horizontal showsHorizontalScrollIndicator={false} >
                {this.props.categories.list.map((g,k) => <TouchableOpacity key={k} onPress={() => this.toggleCategory(g)} style={[styles.category, {backgroundColor : this.state.filter === g? colors.primary : 'rgba(90, 73, 223, .4)'}]} ><Text style={{color : 'white'}}>{g}</Text></TouchableOpacity>)}
              </ScrollView>
            }
          </View>
        }
        
        { this.state.loadingSales? 
            <ActivityIndicator size="small" color={"white"} style={{marginVertical:10}}/>
          :  
            <FlatList
              refreshControl={
                <RefreshControl
                  tintColor="white"
                  refreshing={this.state.refreshing}
                  onRefresh={this.onRefresh}
                />
              }
              onEndReachedThreshold={0}
              onEndReached={this.getMore}
              data={(this.state.filter == 'Todas')? this.props.sales.list : this.props.sales.list.filter(i => i.category === this.state.filter)}
              keyExtractor={item => item.id}
              renderItem={({ item }) => 
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('SaleDetail', { id : item.id })}>
                  <View style={styles.item}>
                    <View style={{flexDirection: 'row', padding: 10, flex: 1, position:'absolute', zIndex: 1, backgroundColor: 'rgba(0,0,0,.3)' }}>
                      <View style={{ flex:1, }}><Text style={{ fontWeight: 'bold', color : 'white' }}>{item.category}</Text></View>
                      <View style={{ flex:1, flexDirection: 'row-reverse'}}><Text style={{ fontWeight: 'bold', color : 'white' }}>{item.created_at_human}</Text></View>
                    </View>
                    <Image 
                      source={{uri:item.photos[0].url}} 
                      style={{width : '100%', height : 300}} 
                      />
                    <View style={{flexDirection: 'row-reverse', marginTop: -37}}>
                      
                      <View style={{ backgroundColor: 'rgba(100, 153, 30, 0.80)', paddingVertical: 10, paddingHorizontal: 20, borderTopLeftRadius:10 }}><Text style={{ fontWeight : 'bold', color: 'white', textAlign:'right',  }}>{item.currency} {this.formatter.format(item.price)}</Text></View>
                      <View style={{flex:2}}></View>
                    </View>
                    
                    <Text style={{ flex:1, fontWeight : 'bold', marginHorizontal : 10, marginVertical: 10, fontSize: 16  }}>{item.title}</Text>
                    <View style={{flexDirection: 'row', marginHorizontal: 10, paddingBottom: 10 }}>
                      <View style={{ flex:1, }}><Text style={{ color : 'gray' }}>{item.description.substring(0,100)}...</Text></View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              }
            />
        }
        { this.state.loadingMore &&
          <ActivityIndicator size="small" color={"white"} style={{marginVertical:10}}/>
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    categories  : state.Categories,
    sales       : state.Sales
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCategories   : categories  => dispatch({type : 'FETCH_CATEGORIES', payload : categories}),
    fetchSales        : sales       => dispatch({type : 'FETCH_SALES', payload : sales}),
    addSales          : sales       => dispatch({type : 'ADD_SALES', payload : sales}),
    fetchSalesByTermn : sales       => dispatch({type : 'FETCH_SALES_BY_TERMN', payload : sales}),
    addSalesByTermn   : sales       => dispatch({type : 'ADD_SALES_BY_TERMN', payload : sales}),
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Sales);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.fifth,
  },
  header : {
    backgroundColor : colors.primary,
    paddingTop: 40,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingHorizontal : 0,
    paddingTop: 8,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  categories : {
    paddingVertical:12,
    paddingLeft : 10,
    backgroundColor : colors.fourth
  },
  category : {
    borderRadius : 25,
    paddingHorizontal : 16,
    paddingVertical : 5,
    marginLeft : 10
  },
  item: {
    margin: 20,
    backgroundColor: 'white',
    // borderTopLeftRadius:6,
    // borderTopRightRadius:6,
    borderRadius: 6,
    overflow:'hidden'
  },
  tags : {
    flexDirection : 'row'
  },
  activeTag : {
    borderWidth : 1,
    borderColor : '#79f89e'
  },
  searchBar : {
    flex:10,
    marginHorizontal: 20,
    borderRadius: 20,
    backgroundColor: 'white',
    paddingHorizontal : 20,
    paddingVertical : 10,
    marginVertical :10
  },
  filter : {
    justifyContent:'center',
    paddingRight: 20
  }

});
