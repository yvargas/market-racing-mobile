import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, ImageBackground, ActivityIndicator, Image, RefreshControl, TouchableWithoutFeedback} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { colors, theme }  from '../../assets/theme';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import firebase from 'firebase/app';
import 'firebase/firestore';

import moment from "moment";
import 'moment/locale/es'

class DirectoryByCategory extends React.Component {

  constructor(){
    super();
    this.state = {
      category    : null,
      loading     : true,
      refreshing  : false,
      loadingMore : false,
      limit       : 5,
      startAfter  : null
    }
  }

  componentDidMount() {
    this.setState({category : this.props.route.params.selected}, () => {

      if(!this.props.directory.list.[this.state.category.slug]){
        
        this.getByCategory()
      } else {

        this.setState({loading:false})
      }
    })
  }

  getByCategory = async () => {

    const directory = await firebase
      .firestore()
      .collection('directory')
      .where('category','==', this.state.category.slug)
      .orderBy('name')
      .limit(this.state.limit)
      .get()
      .catch(e => console.log(e));

    const _directory = directory.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))
  
    this.props.fetchDirectory({ category : this.state.category.slug, [this.state.category.slug] : _directory });

    this.setState({ loading: false, startAfter: directory.docs[directory.docs.length-1]})
  }

  onRefresh = async () => {

    this.setState({refreshing : true});

    const directory = await firebase
      .firestore()
      .collection('directory')
      .where('category','==', this.state.category.slug)
      .orderBy('name')
      .limit(this.state.limit)
      .get()
      .catch(e => console.log(e));

    const _directory = directory.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))
  
    this.props.fetchDirectory({ category : this.state.category.slug, [this.state.category.slug] : _directory });

    this.setState({ refreshing: false, startAfter: directory.docs[directory.docs.length-1] })
  };

  getMore = async () => {
      
    if(!this.state.refreshing && this.state.startAfter != undefined){

      this.setState({loadingMore: true, refreshing : true});

      const directory = await firebase
        .firestore()
        .collection('directory')
        .where('category','==', this.state.category.slug)
        .orderBy('name')
        .startAfter(this.state.startAfter)
        .limit(this.state.limit)
        .get()
        .catch(e => console.log(e));

      const _directory = directory.docs.map(p => ({ id: p.id, created_at_human : moment(p.data().created_at.toDate()).startOf('hour').fromNow(), ...p.data() }))

      const all = this.props.directory.list.[this.state.category.slug].concat(_directory)

      this.props.addMore({ category : this.state.category.slug, [this.state.category.slug] : all });

      this.setState({loadingMore: false, refreshing : false, startAfter: directory.docs[directory.docs.length-1]});
    } 
  }

  render(){
    return (
      <View style={styles.container}>
        { this.state.category &&
          <ImageBackground source={this.state.category.image} style={{paddingBottom : 30, paddingTop: 120}}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold', color : 'white', fontSize: 24 }}>{this.state.category.title}</Text>
          </ImageBackground>
        }

        { this.state.loading ?
            <ActivityIndicator size="small" color={"white"} style={{flex:1}}/>
          :
            this.props.directory.list.[this.state.category.slug].length == 0 ?
              <Text style={{ fontWeight: 'bold', color: 'white', textAlign: 'center', padding:20 }}>Aun no tenemos</Text>
            :
              <>
                <FlatList
                  refreshControl={
                    <RefreshControl
                      tintColor="white"
                      refreshing={this.state.refreshing}
                      onRefresh={this.onRefresh}
                    />
                  }
                  style={{ margin: 20 }}
                  onEndReachedThreshold={0}
                  onEndReached={this.getMore}
                  data={ this.props.directory.list.[this.state.category.slug] }
                  keyExtractor={item => item.id}
                  renderItem={({ item }) => 
                    <TouchableWithoutFeedback  onPress={() => this.props.navigation.navigate('DirectoryDetail', { category : this.state.category.slug ,id : item.id })}>
                      <View style={{marginBottom : 20, borderWidth: 1, borderColor : colors.fourth, borderRadius:10, overflow: 'hidden' }}>
                        {console.log(item.image)}
                        <Image 
                          source={{uri:item.image}}
                          style={{width : '100%', height : 140}} 
                          />
                        <View style={{paddingVertical: 20, paddingHorizontal: 20}}>
                          <Text style={{ color : 'white'}}>{item.name}</Text>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  }
                />

                { this.state.loadingMore &&
                  <ActivityIndicator size="small" color={"white"} style={{marginVertical:10}}/>
                }
              </>
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    directory : state.Directory
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchDirectory  : directory  => dispatch({type : 'FETCH_DIRECTORY', payload : directory}),
    addMore         : directory  => dispatch({type : 'ADD_MORE_DIRECTORY', payload : directory}),
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(DirectoryByCategory);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.fifth,
  },
  header : {
    backgroundColor : '#002f50',
    paddingTop: 80,
    paddingBottom: 20,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  item: {
    padding: 20,
    borderBottomWidth : 1,
    borderColor : '#ddd'
  }
});
