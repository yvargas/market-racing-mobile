import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, ImageBackground} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

class DirectoryDetail extends React.Component {

  constructor(){
    super();
    this.state = {
      category : null
    }
  }

  componentDidMount() {

    this.setState({category : this.props.route.params.selected})
  }

  getByCategory = () => {



  }


  render(){
    return (
      <View style={styles.container}>
        { this.state.category &&
          <ImageBackground source={this.state.category.image} style={{paddingBottom : 34, paddingTop: 80}}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold', color : 'white', fontSize: 20 }}>{this.state.category.title}</Text>
          </ImageBackground>
        }
        
      </View>
    );
  }
}

export default DirectoryDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header : {
    backgroundColor : '#002f50',
    paddingTop: 80,
    paddingBottom: 20,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  item: {
    padding: 20,
    borderBottomWidth : 1,
    borderColor : '#ddd'
  }
});
