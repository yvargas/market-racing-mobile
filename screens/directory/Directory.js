import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, ImageBackground, Image, Linking, Alert} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { colors, theme }  from '../../assets/theme';

import financiamiento from '../../assets/directory/financiamiento.jpg';
import { LinearGradient } from 'expo-linear-gradient';

class Tools extends React.Component {

  constructor(){
    super();
    this.state = {
      categories : [
        {
          id:'0',
          title: 'Auto adornos',
          slug: 'auto_adornos',
          image: require('../../assets/directory/auto-adornos.jpg')
        },
        {
          id:'1',
          title: 'Financiamiento',
          slug: 'financiamiento',
          image: require('../../assets/directory/financiamiento.jpg')
        },
        {
          id:'2',
          title: 'Renta de vehiculos',
          slug: 'rent_car',
          image: require('../../assets/directory/rent-car.jpg')
        },
        {
          id:'3',
          title: 'Repuestos',
          slug: 'repuestos',
          image: require('../../assets/directory/repuestos.jpg')
        },
        {
          id:'4',
          title: 'Seguros',
          slug: 'seguros',
          image: require('../../assets/directory/seguros.jpg')
        },        
        {
          id:'5',
          title: 'Talleres',
          slug: 'talleres',
          image: require('../../assets/directory/talleres.jpg')
        }
      ]
    }
  }

  alert () {
    Alert.alert(
      'Informacion',
      'Quiero que mi negocio este dentro del directorio',
      [
        {
          text: 'Contactar',
          onPress: () => Linking.openURL('mailto:yunkerapp@support.com?subject=Quiero%20ingresar&body=Soy')
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }
      ],
      { cancelable: false }
    );
  }

  render(){
    return (
      <View style={styles.container}>
        <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
          <View style={styles.headerTitleBox}>
            <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
              <View style={{}}></View>
              <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
                <Text style={{ fontSize: 26, textAlign: 'center', color: 'white', fontWeight : 'bold'}}>Directorio</Text>
              </View>
              <View style={{justifyContent: 'center', alignContent:'center'}}>
                <TouchableOpacity onPress={() => this.alert()}>
                  <Ionicons name="ios-information-circle-outline" size={20} color='white' />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </LinearGradient>

        <FlatList
          style={{ padding : 20 }}
          data={this.state.categories}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('DirectoryByCategory', { selected : item })} style={{ marginBottom : 16, borderWidth: 1, borderColor : colors.fourth, borderRadius:10, overFlow: 'hidden' }}>
              <ImageBackground source={item.image} style={{padding : 34}}>
                <Text style={{ textAlign: 'center', fontWeight: 'bold', color : 'white', fontSize: 20 }}>{item.title}</Text>
              </ImageBackground>
            </TouchableOpacity>
          }
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

export default Tools;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.fifth,
  },
  header : {
    backgroundColor : colors.primary,
    paddingTop: 50,
    paddingBottom:10,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  item: {
    padding: 20,
    borderBottomWidth : 1,
    borderColor : '#ddd'
  }
});
