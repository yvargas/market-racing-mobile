import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, Image} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';
import { colors, theme }  from '../../assets/theme';
import { LinearGradient } from 'expo-linear-gradient';

import { ProgressBar, Colors } from 'react-native-paper';

import firebase from 'firebase/app';
import 'firebase/auth';

class Profile extends React.Component {

  constructor(){
    super();
    this.state = {
      
    }
  }

  componentDidMount() {

  }

  handleSignOut = async () => firebase.auth().signOut()

  render(){
    return (
      <View style={styles.container}>
        <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
          <View style={styles.headerTitleBox}>
            <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
              <View style={{flex:1}}></View>
              <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
                <Image source={require('../../assets/junker.png')} style={{width : 110, height : 20}}/>
              </View>
              <View style={{flex:1}}></View>
            </View>
          </View>
        </LinearGradient>

        { !this.props.auth.phoneNumber &&
          <View style={{ backgroundColor: 'yellow', padding : 20 }}>
            <Ionicons name="ios-warning" size={30} color='black' style={{ textAlign : 'center', marginBottom : 10}} />
            <Text style={{color : 'black', textAlign: 'center'}}> Complete su numero de celular para que pueda ser contactado !</Text>
          </View>
        }

        { this.props.auth.displayName &&
          <View style={{ marginVertical: 10, marginHorizontal : 20}}>
            <Text style={{ textAlign: 'center', color: 'white', fontSize: 26, fontWeight: 'bold' }}>{this.props.auth.displayName}</Text>
          </View>
        }

        
        { this.props.auth.sales &&
          <View style={{ marginVertical: 20, marginHorizontal : 20}}>
            <Text style={{ textAlign: 'center', marginVertical: 6, color : 'white'}}>Publicaciones</Text>
            <View style={{ flexDirection: 'row' , marginVertical: 6}}>
              <Text style={{color : 'white', flex:1}}>{this.props.auth.sales.length}</Text>
              <Text style={{color : 'white', flex:1, textAlign: 'right'}}>{this.props.auth.salesLimit}</Text>
            </View>
            <ProgressBar progress={this.props.auth.sales.length/this.props.auth.salesLimit} color={(this.props.auth.sales.length == this.props.auth.salesLimit)? 'red' : colors.primary} />
          </View>
        }

        <FlatList
          style={{ margin: 20 }}
          data={ this.props.auth.sales }
          keyExtractor={item => item.id}
          renderItem={({ item }) => 
            <TouchableOpacity onPress={() => this.props.navigation.navigate('EditSale', { id : item.id })}>
              <View style={{marginBottom : 20, borderBottomWidth: 1, borderColor : colors.fourth, overflow: 'hidden' }}>
                <View style={{marginBottom : 14}}>
                  <Text style={{ color : 'white'}}>{item.title}</Text>
                </View>
              </View>
            </TouchableOpacity>
          }
        />

        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileEdit')} style={{backgroundColor : colors.primary, padding : 10, alignItems:'center', marginHorizontal : 20, borderRadius : 10}}>
          <Text style={{color : 'white'}}>Editar mi perfil</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.handleSignOut()} style={{backgroundColor : 'red', padding : 10, alignItems:'center', margin : 20, borderRadius : 10}}>
          <Text style={{color : 'white'}}>SALIR</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth  : state.Auth,
    stats : state.Stats
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signOut   : user => dispatch({type : 'INIT_AUTH_STATE'}),
    getStats  : data => dispatch({type : 'USER_STATS'})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.fifth,
  },
  header : {
    paddingTop: 50,
    paddingBottom: 10,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  item: {
    padding: 20,
    borderBottomWidth : 1,
    borderColor : '#ddd'
  }
});
