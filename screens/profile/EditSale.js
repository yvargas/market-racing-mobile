import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, TextInput, Switch, KeyboardAvoidingView, TouchableWithoutFeedback, ActivityIndicator, Keyboard, ScrollView, Image, Dimensions} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import SwitchSelector from "react-native-switch-selector";

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import { colors, theme }  from '../../assets/theme';
import { LinearGradient } from 'expo-linear-gradient';

import { useForm, Controller } from 'react-hook-form';

import RNPickerSelect from 'react-native-picker-select';

import { Constants } from 'expo';
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'

import { ActionSheet } from 'react-native-cross-actionsheet'

import { ProgressBar, Colors } from 'react-native-paper';

import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

import moment from "moment";

const { width } = Dimensions.get('window');
const height = width * 0.8;

const EditSale = props => {

  const { control, register, handleSubmit, errors, reset, setValue } = useForm();

  const categories = props.categories.list.map(c => ({ label : c, value : c }))

  const [sale, setSale]                   = useState({});
  const [exist, setExist]                 = useState(false);
  const [loading, setLoading]             = useState(false);
  const [loadingL, setLoadingL]           = useState(true);
  const [category, setCategory]           = useState('');
  const [categoryError, setCategoryError] = useState(false);
  const [error, setError]                 = useState(null);

  const [condition, setCondition]         = useState('Nuevo');
  const [currency, setCurrency]           = useState('RD');

  const [image, setImage]                 = useState(null);
  const [uploading, setUploading]         = useState(false);
  const [uploadPercent, setUploadPercent] = useState(0);
  const [photosFolder, setPhotosFolder]   = useState(moment().format('YYYY-MM-DD_HH:mm'));
  const [photos, setPhotos]               = useState([]);

  const [,setState]                       = useState();

  if(!props.auth.phoneNumber){

    alert('Registre su numero para que pueda ser contactado!')
    props.navigation.navigate('Perfil');
  }

  useEffect(() => {

    const saleIndex = props.sales.list.findIndex(s => s.id === props.route.params.id)

    if(saleIndex !== -1){

      setSale(props.sales.list[saleIndex])
      setPhotos(props.sales.list[saleIndex].photos)
      setPhotosFolder(props.sales.list[saleIndex].photosFolder)
      setCategory(props.sales.list[saleIndex].category)
      setValue('title',props.sales.list[saleIndex].title)
      setValue('price',props.sales.list[saleIndex].price)
      setValue('description',props.sales.list[saleIndex].description)
      setExist(true)

    } else {

      firebase
      .firestore()
      .collection('sales')
      .doc(props.route.params.id)
      .get()
      .then(doc => {

        if(doc.exists){

          setSale(doc.data())
          setPhotos(props.sales.list[saleIndex].photos)
          setPhotosFolder(props.sales.list[saleIndex].photosFolder)
          setCategory(props.sales.list[saleIndex].category)
          setValue('title',props.sales.list[saleIndex].title)
          setValue('price',props.sales.list[saleIndex].price)
          setValue('description',props.sales.list[saleIndex].description)
          setExist(true)

        } else {

          setExist(false)
        }
      });
    }
  }, [sale]);

  // upload image
  const pickPhoto = async () => {
    const {
      status: cameraRollPerm
    } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    // only if user allows permission to camera roll
    if (cameraRollPerm === 'granted') {
      let pickerResult = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Photo,
        allowsEditing: true,
        aspect: [4, 3],
      });

      if(!pickerResult.cancelled){
        uploadPhoto(pickerResult);
      }
    }
  };

  const takePhoto = async () => {
    const {
      status: cameraRollPerm
    } = await Permissions.askAsync(Permissions.CAMERA);

    // only if user allows permission to camera
    if (cameraRollPerm === 'granted') {
      let pickerResult = await ImagePicker.launchCameraAsync({

      });

      if(!pickerResult.cancelled){
        uploadPhoto(pickerResult);
      }
    }
  };

  const uploadPhoto = async(photo) => {

    setUploading(true)
    setUploadPercent(0)

    var fileUri = (Platform.OS === 'android')? photo.path : photo.uri;

    const response = await fetch(fileUri);
    const blob = await response.blob();

    var fileName = moment().format('MM_DD_YYYY-hh:mm:ss-A');

    var metadata = {
      contentType: 'image/jpeg'
    };

    var ref = 'sales/'+ photosFolder + '/' + fileName + ".jpg";

    var uploadTask = firebase.storage().ref(ref).put(blob, metadata);

    uploadTask.on('state_changed', function(snapshot){
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      setUploadPercent(progress)
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
      }
    }, function(error) {
      // Handle unsuccessful uploads
      console.log(error)
    }, function(result) {

      uploadTask.snapshot.ref.getDownloadURL().then(url => setPhotos(last => (last.length === 0)? [{ref, url}] : [...last, {ref, url}]));
      setUploading(false)
      setUploadPercent(0)
    });

  }

  const publish = async(data, e) => {

    if(photos.length !== 0){

      if(category !== ''){
        await firebase
        .firestore()
        .collection('sales')
        .doc(sale.id)
        .update({
          title : data.title,
          category : category,
          currency : currency,
          price : data.price,
          condition : condition,
          description : data.description,
          created_by : {
            uid           : props.auth.uid,
            name          : props.auth.displayName ? props.auth.displayName : props.auth.email,
            phoneNumber   : props.auth.phoneNumber,
            contactMethod : props.auth.contactMethod
          },
          photos : photos,
          photosFolder : photosFolder,
          keywords : data.title.toLowerCase().split(' ').filter(i => i !== ''),
          status : 'public'
        }).then(r => {

          props.navigation.navigate('Venta')
          // update document on redux

        }).catch(e => {

          console.warn(e)
        })
      } else {

        alert('Por favor selecciona una categoria')
      }

    } else {

      alert('Agrega unas cuantas fotos')
    }
  }

  const removePhoto = (img) => {

    firebase.storage().ref(img.ref).delete().then(() => {

      var newPhotos = photos.filter(p => (p.ref !== img.ref));

      setPhotos(newPhotos);

      // update on firestore
      firebase
        .firestore()
        .collection('sales')
        .doc(sale.id)
        .update({
          photos : newPhotos
        })

    }).catch(function(error) {
      // Uh-oh, an error occurred!
    });

  }

  const photoOptions = () => {

    ActionSheet.options({
      title: 'Seleccinar imagen',
      message: 'Desde donde quieres seleccionarla',
      options: [
        { text: 'Tomar Foto', onPress: () => takePhoto() },
        { text: 'Seleccionar del carrete', onPress: () => pickPhoto() },
      ],
      cancel: { onPress: () => console.log('cancel') }
    })
  }
  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss() }>
        <>
        <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
          <View style={styles.headerTitleBox}>
            <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
              <View style={{flex:1}}></View>
              <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
                <Image source={require('../../assets/junker.png')} style={{width : 110, height : 20}}/>
              </View>
              <View style={{flex:1}}></View>
            </View>
          </View>
        </LinearGradient>
        <View style={{ backgroundColor: 'yellow', padding: 20 }}>
          <Text>Editar</Text>
        </View>

        { exist ?
            <ScrollView style={{ marginTop: 20, marginHorizontal: 20 }} keyboardDismissMode='interactive' keyboardShouldPersistTaps='never' showsVerticalScrollIndicator={false}>
              { props.auth.sales.length == props.auth.salesLimit ? 
                  <View>
                    <Text style={{ textAlign: 'center', marginVertical :10 }}>has alcanzado el limite de post</Text>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Perfil')} style={{backgroundColor : colors.primary, padding : 10, alignItems:'center', marginHorizontal : 20, borderRadius : 10}}>
                      <Text style={{color : 'white'}}>Ir al perfil</Text>
                    </TouchableOpacity>
                  </View>
                :
                  <KeyboardAvoidingView behavior="height" enabled={true}>
                    <Controller
                      control={control}
                      render={({ onChange, onBlur, value }) => (                    
                        <TextInput
                          style={ styles.inputText }
                          placeholder="Titulo"
                          onBlur={onBlur}
                          onChangeText={value => onChange(value)}
                          defaultValue={sale.title}
                        />
                      )}
                      name="title"
                      rules={{ required: true }}
                      defaultValue=""
                    />
                    {errors.title && <Text style={{color : 'red'}}>Campo requerido</Text>}

                    <RNPickerSelect
                      onValueChange={(value) => setCategory(value)}
                      style={{
                        inputIOS: {
                          fontSize: 16,
                          paddingVertical: 12,
                          // paddingHorizontal: 10,
                          borderBottomWidth : 1,
                          borderBottomColor: '#ccc',
                          borderRadius: 4,
                          color: 'black',
                        },
                        inputAndroid: {
                          fontSize: 16,
                          // paddingHorizontal: 10,
                          paddingVertical: 8,
                          borderBottomWidth : 1,
                          borderBottomColor: 'purple',
                          borderRadius: 8,
                          color: 'black',
                        },
                      }}
                      value={category}
                      useNativeAndroidPickerStyle={false}
                      items={categories}
                    />
                    
                    <View style={{ flexDirection : 'row', alignContent: 'center', marginVertical: 20}}>
                      <SwitchSelector
                        value={(sale.currency == 'US')? 0 : 1}
                        initial={(sale.currency == 'US')? 0 : 1}
                        onPress={value => setCurrency(value)}
                        textColor={colors.primary}
                        selectedColor={colors.white}
                        buttonColor={colors.purple}
                        borderColor={colors.purple}
                        hasPadding
                        style={{ flex : 1}}
                        options={[
                          { label: "US", value: "US"},
                          { label: "RD", value: "RD"}
                        ]}
                      />
                      
                      <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                          <TextInput
                            style={ [styles.inputText, {textAlign: 'right', flex : 2, marginLeft : 20}] }
                            placeholder="Precio"
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            defaultValue={sale.price}
                            keyboardType='numeric'
                          />
                        )}
                        name="price"
                        rules={{ required: true }}
                        defaultValue=""
                      />
                      
                    </View>
                    {errors.price && <View style={{ marginBottom: 20, marginTop: -20}}><Text style={{color : 'red', textAlign: 'right'}}>Campo requerido</Text></View>}

                    <SwitchSelector
                      value={(sale.condition == 'Usado')? 0 : 1}
                      initial={(sale.condition == 'Usado')? 0 : 1}
                      onPress={value => setCondition(value)}
                      textColor={colors.primary}
                      selectedColor={colors.white}
                      buttonColor={colors.purple}
                      borderColor={colors.purple}
                      hasPadding
                      style={{marginBottom : 20}}
                      options={[
                        { label: "Usado", value: "Usado"},
                        { label: "Nuevo", value: "Nuevo"}
                      ]}
                    />

                    <Controller
                      control={control}
                      render={({ onChange, onBlur, value }) => (
                        <View style={styles.textAreaContainer} >
                          <TextInput
                            style={ styles.textArea }
                            placeholder="Descripcion"
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            defaultValue={sale.description}
                            multiline={true}
                            numberOfLines={2}
                          />
                        </View>
                      )}
                      name="description"
                      rules={{ required: true }}
                      defaultValue=""
                    />
                    {errors.description && <Text style={{color : 'red'}}>Campo requerido</Text>}

                    <View style={{flexDirection : 'row', marginVertical : 20}}>
                      {photos.map((g,k) => <View key={k} ><TouchableOpacity style={{backgroundColor: 'red', width: 26, height: 26, position: 'absolute', zIndex : 1, justifyContent: 'center', alignItems: 'center', right : 0, top: -10, borderRadius: 20}} onPress={() => removePhoto(g)}><Text style={{color : 'white'}}>x</Text></TouchableOpacity><Image key={k} source={{uri:g.url}} style={{marginHorizontal : 2, flex:1, width : (width - 32) / 3 , height : (height - 32) / 3}} /></View>)}
                    </View>

                    { uploading &&
                      <ProgressBar progress={uploadPercent} color={colors.primary} />
                    }

                    { photos.length <= 2 &&
                      <TouchableOpacity onPress={() => photoOptions()} style={{backgroundColor : (uploading)? colors.fourth : colors.primary, padding : 10, alignItems:'center', marginTop : 20, borderRadius : 10}} disabled={uploading}>
                        {uploading ? (
                            <ActivityIndicator size="small" color={"white"} />
                          ) : (
                            <Text style={{color : 'white'}}><Ionicons name="ios-camera" size={20} color='white' /></Text>
                          )
                        }
                      </TouchableOpacity>
                    }

                    <TouchableOpacity onPress={handleSubmit(publish)} style={{backgroundColor : colors.third, padding : 10, alignItems:'center', marginTop : 20, borderRadius : 10}} disabled={uploading}>
                      <Text style={{color : 'white'}}>ACTUALIZAR</Text>
                    </TouchableOpacity>
                  </KeyboardAvoidingView>
              }
            </ScrollView>
          :
            <Text style={{ textAlign: 'center' }}>Ops, Este post no Existe</Text>
          }
        </>
      </TouchableWithoutFeedback>
    </View>
  );
}

const mapStateToProps = state => {
  return {
    auth        : state.Auth,
    categories  : state.Categories,
    sales       : state.Sales,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addSale       : sale  => dispatch({type : 'ADD_SALE', payload : sale}),
    incrementSale : sale  => dispatch({type : 'INCREMENT_SALE', payload : sale})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(EditSale);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header : {
    backgroundColor : '#002f50',
    paddingTop: 50,
    paddingBottom: 10,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  inputText: {
    // margin : 20,
    paddingVertical : 10,
    borderBottomWidth : 1,
    borderColor : 'rgba(0,0,0,0.2)',
    marginBottom : 20,
    color : 'black'
  },
  textAreaContainer: {
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 5
  },
  textArea: {
    height: 100,
    justifyContent: "flex-start"
  },

  exampleText: {
    fontSize: 20,
    marginBottom: 20,
    marginHorizontal: 15,
    textAlign: 'center',
  },
  maybeRenderUploading: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
  },
  maybeRenderContainer: {
    borderRadius: 3,
    elevation: 2,
    marginTop: 30,
    shadowColor: 'rgba(0,0,0,1)',
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 4,
      width: 4,
    },
    shadowRadius: 5,
    width: 250,
  },
  maybeRenderImageContainer: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    overflow: 'hidden',
  },
  maybeRenderImage: {
    height: 250,
    width: 250,
  },
  maybeRenderImageText: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  }
});
