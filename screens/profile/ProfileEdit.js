import  React, { useState } from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity, TextInput, ActivityIndicator, KeyboardAvoidingView, Keyboard, Image} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import { colors, theme }  from '../../assets/theme';
import { LinearGradient } from 'expo-linear-gradient';

import { useForm, Controller } from 'react-hook-form';

import RNPickerSelect from 'react-native-picker-select';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const Profile = props => {

  const { control, handleSubmit, errors } = useForm({
    defaultValues: {
      displayName   : props.auth.displayName,
      email         : props.auth.email,
      phoneNumber   : props.auth.phoneNumber,
      instagram     : props.auth.instagram,
    }
  });

  const [loading, setLoading]                       = useState(false);
  const [contactMethod, setContactMethod]           = useState(props.auth.contactMethod);
  const [contactMethodError, setContactMethodError] = useState(false);
  const [error, setError]                           = useState(null);
  
  const onSubmit = async(data) => {
    if(contactMethod) {
      setLoading(true);

      var user = firebase.auth().currentUser;

      user.updateProfile({
        displayName   : data.displayName,
        email         : data.email,
        phoneNumber   : data.phoneNumber
      }).then(function() {
        
        // update database
        return firebase
        .firestore()
        .collection('users')
        .doc(user.uid)
        .update({
          displayName : data.displayName,
          phoneNumber : data.phoneNumber, 
          email       : data.email,
          instagram   : data.instagram,
          contactMethod : contactMethod
        })

      }).then(function() {

        setLoading(false);

        // Update user.
        return props.updateProfile({
          loggedIn      : true, 
          uid           : user.uid, 
          emailVerified : user.emailVerified, 
          displayName   : data.displayName, 
          phoneNumber   : data.phoneNumber, 
          instagram     : data.instagram, 
          email         : data.email, 
          contactMethod : contactMethod 
        })

      }).catch(error => {
        // An error happened.
        console.log(error)
      });
    } else {
      setContactMethodError(true)
    }
  }

  return (
    <View style={styles.container}>
      <LinearGradient colors={[colors.third,colors.primary]} start={{x: 0, y: 0}} end={{x: 0, y: 2.5}} style={styles.header}>
        <View style={styles.headerTitleBox}>
          <View style={{flexDirection: 'row', paddingVertical:10, paddingHorizontal : 20}}>
            <View style={{flex:1}}></View>
            <View style={{flex:1, justifyContent : 'center', alignContent:'center'}}>
              <Image source={require('../../assets/junker.png')} style={{width : 110, height : 20}}/>
            </View>
            <View style={{flex:1}}></View>
          </View>
        </View>
      </LinearGradient>
      <View style={{ margin : 20, flex: 1 }}>
        <View style={{ paddingVertical : 20, marginBottom: 20 }}>
          <Text style={{ textAlign: 'center', color : colors.primary }}>Seccion para actualizar perfil</Text>
        </View>
        <KeyboardAvoidingView behavior="position" enabled>
          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                style={ styles.inputText }
                placeholder="Nombre"
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
              />
            )}
            name="displayName"
            rules={{ required: true }}
            defaultValue=""
          />
          {errors.displayName && <Text style={{color : 'red'}}>Campo requerido</Text>}

          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                style={ styles.inputText }
                placeholder="Email"
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
              />
            )}
            name="email"
            rules={{ required: true }}
            defaultValue=""
          />
          {errors.email && <Text style={{color : 'red'}}>Campo requerido</Text>}

          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                style={ styles.inputText }
                placeholder="Celular"
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                keyboardType="numeric"
              />
            )}
            name="phoneNumber"
            rules={{ required: true,  }}
            defaultValue=""
          />
          {errors.phoneNumber && <Text style={{color : 'red'}}>Campo requerido</Text>}

          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                style={ styles.inputText }
                placeholder="@junkerApp"
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
              />
            )}
            name="instagram"
            rules={{  }}
            defaultValue=""
          />

          <RNPickerSelect
            onValueChange={(value) => setContactMethod(value)}
            placeholder={{
              label: 'Metodo de contacto',
              value: null,
            }}
            style={ styles.inputText }
            useNativeAndroidPickerStyle={false}
            value={contactMethod}
            items={[
              { label: 'Solo Llamadas', value: 'calls' },
              { label: 'Llamadas y Whatsapp', value: 'whatsapp' }
            ]}
          />
          {contactMethodError && <Text style={{color : 'red'}}>Campo requerido</Text>}
        </KeyboardAvoidingView>
      </View>

      <TouchableOpacity style={ [{ padding : 10, backgroundColor : 'rgba(100, 153, 30, 0.80)', alignItems:'center', marginHorizontal : 20, borderRadius : 10 , marginBottom : 20}] } onPress={handleSubmit(onSubmit)}>
        {loading ? (
            <ActivityIndicator size="small" color={"white"} />
          ) : (
            <Text style={{color : 'white'}}>Actualizar</Text>
          )
        }
      </TouchableOpacity>
    </View>
  );
}

const mapStateToProps = state => {
  return {
    auth : state.Auth
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateProfile : user => dispatch({type : 'UPDATE_AUTH_STATE', payload : user}),
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header : {
    // height : 197,
    backgroundColor : '#002f50',
    paddingTop: 50,
    paddingBottom: 10,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  inputText: {
    // margin : 20,
    paddingVertical : 10,
    borderBottomWidth : 1,
    borderColor : 'rgba(0,0,0,0.2)',
    marginBottom : 20,
    color : 'black'
  },
});
