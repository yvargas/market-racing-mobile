import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { colors, theme }  from '../../assets/theme';


import * as firebase from 'firebase/app';
import 'firebase/auth';

const VerifyEmail = props => {

  goToLogin = async () => firebase.auth().signOut().then(() => props.navigation.navigate('Login'))

  return (
    <ImageBackground source={require('../../assets/auth-bg.jpg')} style={styles.imageContainer} >
      <View style={ styles.container }>
        <Text style={{textAlign : 'center'}}>Por favor verifica tu email, luego haz login !</Text>
        <TouchableOpacity onPress={() => goToLogin()} style={{color : 'red'}}>
          <Text style={{color : 'red'}}>Ir al Login</Text>
        </TouchableOpacity>
      </View>
      
    </ImageBackground>
  );
  
}

export default VerifyEmail;

const styles = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : 'rgba(255,255,255, .6)',
    justifyContent : 'center',
    paddingHorizontal : 40
  },
  imageContainer : {
    width: '100%',
    height: '100%',
  },
  inputText: {
    marginTop : 20,
    borderBottomWidth : 1,
    borderColor : 'white',
    marginBottom : 20,
    color : 'white'
  },
  loginText : {
    fontSize : 40,
    textAlign: 'center',
    color : 'white'
  },
  whiteTxt : {
    // color : 'white'
  },
  primaryTxt : {
    color : 'purple'
  },
  registerBox : {
    paddingTop: 20,
    justifyContent: 'center',
    flexDirection: 'row'
  }
});
