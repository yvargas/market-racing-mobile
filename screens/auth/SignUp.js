import React, { useState } from 'react';
import { StyleSheet, Text, View, StatusBar, TextInput, Image, TouchableOpacity, ActivityIndicator, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import { connect } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { colors, theme }  from '../../assets/theme';


import * as firebase from 'firebase/app';
import 'firebase/auth';

const SignUp = props => {

  const [email, setEmail]       = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading]   = useState(false);
  const [error, setError]       = useState(null);

  const { control, handleSubmit, errors } = useForm();
  const onSubmit = async(data) => {
    setLoading(true);
    try {

      await firebase.auth().createUserWithEmailAndPassword(data.email,data.password).then(() => firebase.auth().currentUser.sendEmailVerification().then(() => props.navigation.navigate('VerifyEmail')));

    } catch (error) {
      setLoading(false);
      setError(error.message);

    }
  }  
  
  return (
    <ImageBackground source={require('../../assets/montain.jpg')} style={styles.imageContainer} >
      <View style={ styles.container }>
        <KeyboardAvoidingView behavior="position" enabled>
          <Text style={styles.loginText}>Registrarse</Text>
          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                style={ styles.inputText }
                placeholder="Email"
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
              />
            )}
            name="email"
            rules={{ required: true }}
            defaultValue=""
          />
          {errors.email && <Text style={{color : 'red'}}>Campo requerido</Text>}
          <Controller
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                style={ styles.inputText }
                placeholder="Clave"
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                secureTextEntry
              />
            )}
            name="password"
            rules={{ required: true }}
            defaultValue=""
          />
          {errors.password && <Text style={{color : 'red'}}>Campo requerido</Text>}
          { error &&
            <Text style={[styles.whiteTxt,{textAlign : 'center'}]}>{error}</Text>
          }
          <TouchableOpacity style={ [theme.btnPrimary, { marginTop : 20 }] } onPress={handleSubmit(onSubmit)}>
            {loading ? (
                <ActivityIndicator size="small" color={"white"} />
              ) : (
                <Text style={styles.whiteTxt}>Registrarme</Text>
              )
            }
          </TouchableOpacity>
          <TouchableOpacity onPress={() => props.navigation.navigate('Login')} style={styles.registerBox}>
            <Text style={styles.whiteTxt}>Volver atras</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </View>
      
    </ImageBackground>
  );
  
}

export default SignUp;

const styles = StyleSheet.create({
  container : {
    flex : 1,
    backgroundColor : 'rgba(255,255,255, .6)',
    justifyContent : 'center',
    paddingHorizontal : 40
  },
  imageContainer : {
    width: '100%',
    height: '100%',
  },
  inputText: {
    marginTop : 20,
    borderBottomWidth : 1,
    borderColor : 'white',
    marginBottom : 20,
    color : 'white'
  },
  loginText : {
    fontSize : 40,
    textAlign: 'center',
    color : 'white'
  },
  whiteTxt : {
    // color : 'white'
  },
  primaryTxt : {
    color : 'purple'
  },
  registerBox : {
    paddingTop: 20,
    justifyContent: 'center',
    flexDirection: 'row'
  }
});
