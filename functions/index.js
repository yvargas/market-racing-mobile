const functions = require('firebase-functions');
const admin = require('firebase-admin');
const { TelegramClient } = require('messaging-api-telegram');
admin.initializeApp();

exports.onCreateUser = functions.auth.user().onCreate((user) => {
  
	return admin
    .firestore()
    .collection('users')
    .doc(user.uid)
    .set({ email : user.email, sales : 0, salesLimit : 5 })
    .then(r => {

      const client = TelegramClient.connect('1135027513:AAEjKEktABAnvEjr-7hmi8FAmd8H_x6x2x4');

      return client.sendMessage('-464746799', 'Nuevo post usuario registrado : ' + user.email, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
    });
  
});

exports.onCreateSale = functions.firestore.document('sales/{saleId}').onCreate((snap, context) => {
  
	const data = snap.data();

  const client = TelegramClient.connect('1135027513:AAEjKEktABAnvEjr-7hmi8FAmd8H_x6x2x4');

  client.sendMessage('-464746799', 'Nuevo post creado por : ' + data.created_by.name + ". Titulo : " + data.title, {
    disable_web_page_preview: true,
    disable_notification: true,
  });

  return;
  
});