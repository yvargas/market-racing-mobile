import * as firebase from 'firebase/app';
import 'firebase/auth';

const app = firebase.initializeApp({
    apiKey: "AIzaSyAc046gKP5OukJmk1_EBVY8fSY07vTJMxA",
    authDomain: "market-racing.firebaseapp.com",
    databaseURL: "https://market-racing.firebaseio.com",
    projectId: "market-racing",
    storageBucket: "market-racing.appspot.com",
    messagingSenderId: "852481316258",
    appId: "1:852481316258:web:1688e7110fd0e50fcfcefb",
    measurementId: "G-F530VGPV4Y"
});

export default app;